require_pkgs:
  pkg.installed:
    - pkgs:
        - apt-transport-https
        - ca-certificates
        - curl
        - software-properties-common
    - refresh: True

# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkgrepo.html

{% if grains['lsb_distrib_codename'] == "xenial" %}
docker_repo:
  pkgrepo.managed:
    - name: deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable
    - gpgcheck: 1
    - key_url: https://download.docker.com/linux/ubuntu/gpg
    - refresh: True
{% endif %}

# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html#salt.states.pkg.installed

docker-ce:
  pkg.installed:
    - refresh: True
    - cache_valid_time: 300
#  service.running:
#    - enable: True
#     - require:
#       - pkg: docker-ce

# salt-ssh kube-minion state.apply kube-installation.docker-install
