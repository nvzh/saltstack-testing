# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkgrepo.html

{% if grains['lsb_distrib_codename'] == "xenial" %}
base:
  pkgrepo.managed:
    - file: /etc/apt/sources.list.d/kubernetes.list
    - name: deb http://apt.kubernetes.io/ kubernetes-xenial main
    - gpgcheck: 1
    - key_url: https://packages.cloud.google.com/apt/doc/apt-key.gpg
    - refresh: True
{% endif %}

kube_pkgs:
  pkg.installed:
    - pkgs:
      - kubeadm
      - kubelet
      - kubectl

# salt-ssh kube-minion state.apply kube-installation.kube-install test=True
