include:
  - kube-installation.preparation
  - kube-installation.docker-install
  - kube-installation.kube-install

# destination node (visudo): nvzh  ALL=(ALL) NOPASSWD: ALL
#
# On main host run # vim /etc/salt/roster
# kube-minion:
#  host: 172.26.78.64
#  user: nvzh
#  passwd: 403966
#  sudo: True
#
# Then run # salt-ssh -i kube-minion test.ping
# And finaly run # salt-ssh \* state.highstate test=True
#
# salt-ssh kube-minion system.reboot
