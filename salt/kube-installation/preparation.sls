# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

software_upgrade:
  pkg.uptodate:
    - refresh: True

# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.network.html#salt.states.network.managed

{% if grains['os'] == "Ubuntu" %}
enp0s3:
  network.managed:
    - type: eth
    - enabled: True
    - proto: static
    - ipaddr: 172.26.78.226
    - netmask: 255.255.255.0
    - gateway: 172.26.78.1
    - dns:
      - 8.8.8.8
      - 8.8.4.4
{% endif %}

Hostname_substitute:
  cmd.run:
    - name: /bin/echo "kube-minion" > /etc/hostname


# salt-ssh kube-minion1 state.sls kube-installation.preparation test=True
# salt-ssh kube-minion1 state.apply kube-installation test=True
# salt-ssh kube-minion1 system.reboot
