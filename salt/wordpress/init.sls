include:
  - wordpress.update-pkgs
  - wordpress.pre-install
  - wordpress.repos-add
  - wordpress.nginx_mariadb-install
  - wordpress.mariadb_prep
  - wordpress.wp-install
