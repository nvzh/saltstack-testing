# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html#salt.states.file.absent

prune_nginx_folder:
  file.absent:
    - name: /usr/share/nginx/html/50x.html

prune_nginx_folder2:
  file.absent:
    - name: /usr/share/nginx/html/index.html

# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html#salt.states.file.managed
# https://www.shellhacks.com/salt-download-file-unknown-source-hash/
# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.archive.html

{% set source_hash = salt['cmd.shell']('echo "md5=`curl -s "https://wordpress.org/latest.tar.gz" | md5sum | cut -c -32`"') %}

getting_wp:
  archive.extracted:
    - name: /usr/share/nginx/html
    - source: https://wordpress.org/latest.tar.gz
    - source_hash: {{ source_hash }}
  file.managed:
    - name: /usr/share/nginx/html/wordpress/wp-config.php
    - source: salt://wordpress/Files/wp-config.php

# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html#salt.states.file.directory

/usr/share/nginx/html:
  file.directory:
    - user: nginx
    - group: nginx
    - dir_mode: 755
    - recurse:
      - user
      - group
      - mode
