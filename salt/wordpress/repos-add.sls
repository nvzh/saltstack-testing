# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkgrepo.html

nginx_repo:
  pkgrepo.managed:
    - humanname: nginx repo
    - name: nginx
    - baseurl: http://nginx.org/packages/centos/$releasever/$basearch/
    - gpgcheck: 0
    - enable: True


mariadb_repo:
  pkgrepo.managed:
    - humanname: MariaDB
    - name: MariaDB
    - baseurl: http://yum.mariadb.org/10.1/centos7-amd64
    - gpgkey: https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
    - gpgcheck: 1

#update-repos:
#  pkg.uptodate


# sudo salt wp-install state.apply wordpress.repos-add test=True
