nginx_install:
  pkg.installed:
    - fromrepo: "nginx.repo"
    - name: nginx
    - refresh: True
    - cache_valid_time: 300
  service.running:
    - name: nginx
    - enable: True

mariadb_install:
  pkg.installed:
    - name: MariaDB-server
    - refresh: True
    - cache_valid_time: 300
  service.running:
    - name: mariadb
    - enable: True

# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html#salt.states.file.rename

#bkp_nginx_initial_config:
#  file.rename:
#    - name: /etc/nginx/nginx.conf
#    - source: /etc/nginx/nginx.conf.bkp

# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html#salt.states.file.managed

copy_conf_file:
  cmd.run:
    - name: mv /etc/nginx/conf.d/default.conf{,.bkp}
  file.managed:
    - name: /etc/nginx/conf.d/wordpress.conf
    - source: salt://wordpress/Files/wordpress.conf

# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html#salt.states.file.line

php-fpm_user-config-modify:
  file.line:
    - name: /etc/php-fpm.d/www.conf
    - mode: replace
    - match: "user = apache"
    - content: "user = nginx"

php-fpm_group-config-modify:
  file.line:
    - name: /etc/php-fpm.d/www.conf
    - mode: replace
    - match: "group = apache"
    - content: "group = nginx"

# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.service.html#salt.states.service.running

php-fpm:
  service.running:
    - enable: True

# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.firewalld.html

open_port_for_nginx:
  firewalld.present:
    - name: public
    - ports:
      - 80/tcp
      - 22/tcp
      - 4505/tcp
      - 4506/tcp


# sudo salt wp-install state.apply wordpress.nginx_mariadb-install test=True
