# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html#salt.states.pkg.installed

base_pkgs:
  pkg.installed:
    - pkgs:
      - epel-release
      - wget
      - net-tools
      - php-fpm
      - php-mysql
      - MySQL-python
