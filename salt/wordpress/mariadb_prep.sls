# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.mysql_database.html
# https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.mysql.html#module-salt.modules.mysql

wordpress:
  mysql_database.present

# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.mysql_user.html
# https://www.digitalocean.com/community/tutorials/saltstack-infrastructure-creating-salt-states-for-mysql-database-servers
# https://docs.saltstack.com/en/latest/topics/pillar/index.html#the-pillar-get-function

wp-admin:
  mysql_user.present:
    - host: localhost
    - password: {{ salt['pillar.get']('mysql:wp-admin_pw', '') }}

# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.mysql_grants.html#module-salt.states.mysql_grants

grant_privileges:
  mysql_grants.present:
    - grant: all privileges
    - database: wordpress.*
    - user: wp-admin
