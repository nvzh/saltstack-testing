# State below is installing zsh and git packages.
# Link: https://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html#salt.states.pkg.installed
zsh installation:
  pkg.installed:
    - pkgs:
      - zsh
      - git

# Next two States are running Bash commands.
# Link: https://docs.saltstack.com/en/latest/ref/states/all/salt.states.cmd.html#salt.states.cmd.run
Shell changing:
  cmd.run:
    - name: chsh -s $(which zsh)

oh-my-zsh installation:
  cmd.run:
    - name: sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Following State copy file from local Files folder to destination /root folder.
# Link: https://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html#salt.states.file.managed
Add oh-my-zsh config:
  file.managed:
    - name: /root/.zshrc
    - source: salt://oh-my-zsh/Files/.zshrc

# Run: salt '*' state.highstate test=True
